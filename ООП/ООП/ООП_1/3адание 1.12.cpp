#include "pch.h"
#include <iostream>
using namespace std;

int main()
{
	setlocale(LC_ALL, "RUSSIAN");
	double funts;
	cout << "Введите сумму в фунтах: ";
	cin >> funts;
	int oldfunts;
	double defrac;
	oldfunts = static_cast<int>(funts);
	defrac = funts - oldfunts;
	int shillings = static_cast<int> (defrac * 20);
	int pens = (defrac * 20 - shillings) * 12;
	cout << "\nСумма в старой форме: " << oldfunts << '.' << shillings << '.' << pens;

	system("pause");
}
