// Задание 1.1.cpp : конвертер галлонов в кубический фут

#include "pch.h"
#include <iostream>

using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	float galoons, cubefoot;
	cout << "Введите количество галлонов" << "\n";
	cin >> galoons;
	cubefoot = galoons / 7.481;
	cout << "Эквивалентный объем в кубических футах:	" << cubefoot << endl;
	system("pause");
	return 0;
}
