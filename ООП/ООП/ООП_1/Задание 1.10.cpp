#include "pch.h"
#include <iostream>

using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	float funt, shillings, pens, dfunt, dshil;
	const float pr = 2.4, fun = 20, pen = 12, qfunt = 100; // раньше: 1 фунт = 240 пенсов, сейчас: 1 фунт = 100 пенсов => новые пенсы подорожали в 2.4 раза
	cout << "Программа преобразования Валюты Великобритании из старого в новый формат:" << endl;
	cout << "Введите количество фунтов: ";
	cin >> funt;
	cout << "Введите количество шилингов: ";
	cin >> shillings;
	cout << "Введите количество пенсов: ";
	cin >> pens;
	dfunt = funt * fun*pen + pens;
	dshil = shillings * pen;
	dfunt += dshil;
	dfunt /= qfunt;
	dfunt /= pr;
	cout << "Десятичных фунтов: " << dfunt << endl;
	system("pause");
	return(0);
}
