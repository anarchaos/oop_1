// Задание 1.3.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
	int var = 10;
	cout << var << endl;
	var *= 2;
	cout << var-- << endl;
	cout << var << endl;
	system("pause");
	return 0;
}
