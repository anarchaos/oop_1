// Задание 1.4.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include <iomanip>
#include <Windows.h>

using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	cout << setw(10) << "Береза" << endl << "Белая береза" << endl << "Под моим окном" << endl << "Принакрылась снегом," << endl << "Точно серебром." << endl << "\n" << endl << "На пушистых ветках" << endl << "Снежною каймой" << endl << "Распустились кисти" << endl << "Белой бахромой" << endl << "\n" << endl << "И стоит береза" << endl << "В сонной тишине," << endl << "И горят снежинки" << endl << "В золотом огне" << endl << "\n" << endl << "А заря, лениво" << endl << "Обходя кругом" << endl << "обсыпает ветки" << endl << "Новым серебром." << endl << "\n" << "\t" << "С.А.Есенин" << endl;
	system("pause");
	return 0;
}

