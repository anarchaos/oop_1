// Задание 1.6.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	float usd, gbp, chf, dem, jpy;
	cout << "Введите денежную сумму в долларах $" << endl;
	cin >> usd;
	gbp = usd / 1.487;
	chf = usd / 0.172;
	dem = usd / 0.584;
	jpy = usd / 0.00955;
	cout << "$" << usd << " = " << gbp << "(GBP)" << endl;
	cout << "$" << usd << " = " << chf << "(CHF)" << endl;
	cout << "$" << usd << " = " << dem << "(DEM)" << endl;
	cout << "$" << usd << " = " << jpy << "(JPY)" << endl;
	system("pause");
	return 0;
}

