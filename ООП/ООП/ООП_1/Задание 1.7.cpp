// Задание 1.7.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>

using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	float farenheit, celsii;
	cout << "Введите температуру в градусах по Цельсию:" << endl;
	cin >> celsii;
	farenheit = celsii * 1.8 + 32;
	cout << celsii << "°C  = " << farenheit << "°F" << endl;
	system("pause");
	return 0;
}