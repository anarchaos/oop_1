// Задание 1.8.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	int number = 8425785;
	cout << "Москва" << setw(14) << setfill('.') << number << endl;
	system("pause");
	return 0;
}