// Задание 1.9.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>

using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	int a, b, c, d, sum;
	char slash = '/';
	cout << "Введите первую дробь: ";
	cin >> a >> slash >> b;
	cout << "Введите вторую дробь: ";
	cin >> c >> slash >> d;
	cout << a << "/" << b << " + " << c << "/" << d << " = " << ((a*d) + (b*c)) << "/" << (b*d) << endl;
	system("pause");
	return 0;
}