// Задание 2.5.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//
#include "pch.h"
#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	int s;// переменная задающая колличество строк
	int q;// переменная задающая колличество символов "X"

	for (s = 1; s <= 20; s+=2)
	{
		cout << setw(21 - s) << " ";// задается колличество пробелов

		for (q = 1; q <= s; q++) // задается колличество символов
		{
			cout << "X";
		}
		cout << endl;
	}
	system("pause");
	return 0;
}