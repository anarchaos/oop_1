#include "pch.h"
#include <iostream>

using namespace std;

int main(void)
{
	setlocale(LC_ALL, "Russian");
	int numb;
	long fact = 1;
	cout << "Enter a number : ";
	cin >> numb; 
	while (numb != 0)
	{
		for (int j = 1; j <= numb; j++) 
			fact *= j;
		cout << "Factorial is " << fact << endl;
		cout << "Enter a number: ";
		cin >> numb;
		fact = 0;
	}
	cout << "Wrong Number!!! " << endl;
	return 0;
}