#include "pch.h"
#include <iostream>

using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	int i;
	float res, cash, years, percent, a;
	cout << "Введите начальный вклад: " << endl;
	cin >> cash;
	cout << "Введите число лет: " << endl;
	cin >> years;
	cout << "Введите процентную ставку: " << endl;
	cin >> percent;
	res = cash;
	for (i = 1; i <= years; i++)
		{
		a = res / 100 * percent;
		res += a;
		}
	cout << "По истечению срока вашего вклада вы получите $" << res << endl;
	return 0;
}


